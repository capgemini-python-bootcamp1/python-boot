class Company:
    # Task 1: Implement an __init__ method for this class
    # Instances of the class should have the attributes: company_name, country_of_origin, headquarter_location, founding_year

    ...


# Task 2: Implement a child class of Company called ItCompany. Implement an __init__ method which additionally sets the attribute: area
# Do not copy the code from Company but call it instead.
class ItCompany: ...


# Task 3: Implement a method for Company, so that when casting Company to str ( str(Company()) ), the str is: "<company_name>, founded in <country_of_origin> in <founding_year> has its headquarter in <headquarter_location>."

# Task 4: Implement a method for Company, so that companies can be compared to each other. The founding_year attribute should be used for comparison. Older companies > younger companies.

# Task 5: Rewrite this code so that no global variable is altered.

global_count = 0


class CharCounter:
    def __init__(self, text: str):
        global global_count
        global_count += len(text)

    @classmethod
    def global_count_as_str(cls):
        return f"Counted {global_count} chars!"


# Bonus: Task 6: Implement an attribute (for Company) that is calculated on the fly when accessed (property decorator).
# The attribute should be called "years_in_business" and should calculate for how many years the company exists


# Bonus: Task 7: Implement an enum called Area which only allows the values: consulting, software and cloud#
class Area: ...
