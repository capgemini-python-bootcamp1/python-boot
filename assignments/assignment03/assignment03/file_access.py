# -*- coding: utf-8 -*-

import csv
import json
from pathlib import Path

"""
HINT: For these assignments the unittests might also contain the solution. So only have a look into them if you really got stuck
"""


def csv_to_json():
    """Task01
    Read in the cve file 'userdata.cve' in the data folder
    Create a dict from the data where the first column "username" is the key and the other columns are the value in dict format such that the column name is the key
    Write the dict as json in a new file called 'userdata.json' in the same folder 

    Hint: running the unittests of this assignment will reset the data directory before and after the tests.

    Writes the file assignment03/data/userdata.json

    Returns:
        none

    """

    ...


def fun_with_paths() -> tuple[Path]:
    """Task02
    Use the python pathlib library to get information from the file system

    Returns: 
      path: the absolute path of this module, e.g. "C:\python-bootcamp\assignments\assignment03\assignment03\"
      path: the relative path from this module to lecture03.pptx, e.g. "..\..\lectures\lecture03.pptx"
      path: the combined path from the previous two, e.g. "C:\python-bootcamp\assignments\assignment03\assignment03\..\..\lectures\lecture03.pptx"
    """

    ...


def fun_with_paths_part2() -> tuple[list[Path], set[str]]:
    """Task03
    Use the python pathlib library to get information from the file system

    Returns:
      list[path]: a list of all files (including their absolut path) in this assignemt03 directory which do not have the .py suffix (do not include the __pycache__ directory!)
      set[str]: a set of all file endings of the found files, e.g. [".yaml", ".json"] - Hint: no empty strings
    """

    ...


def implement_walk_function(path: Path) -> tuple[Path, list[str], list[str]]:
    """Task05 (optional)
    Implement the python walk function: https://docs.python.org/3/library/pathlib.html#pathlib.Path.walk
    Parameters of the walk function can be ignored 
    """

    ...
