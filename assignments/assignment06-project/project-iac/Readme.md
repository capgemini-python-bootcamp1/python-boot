
# Installation & Getting Started
- [Install pulumi](https://www.pulumi.com/docs/install/)
- Create an account at pulumi, you can use your GitLab as auth provider
- [Pulumi DigitalOcean docs](https://www.pulumi.com/registry/packages/digitalocean/)
- [Pulumi DigitalOcean repo](https://github.com/pulumi/pulumi-digitalocean)
- [DigitalOcean blog post about what we want to do](https://www.digitalocean.com/community/tutorials/how-to-manage-digitalocean-and-kubernetes-infrastructure-with-pulumi)

## DigitalOcean
DigitalOcean is a lean cloud provider with a focus on k8s.
There is a 200$ budget when you create a new account. Please **READ CAREFULLY** the information on the providers website. It should be possible to do this project solely with this budget so you do not get charged anything on top!!!
For this reason it is absolutely crucial to choose the right sizing (as small as possible) and to tear down your infrastructure when you don't need it

Other Cloud Provider: It is perfectly fine to use a cloud provider of your choise instead of DigitalOcean. Please make sure that no additional costs for you or the bootcamp come up. If you choose another provider the help I can provide might be very limited. 

# Project Requirements
As base requirements you are supposed to create the following infrastructure components with pulumi in digital ocean
- container registry
- kubernetes cluster
- database
- database backup
- spaces object storage

all resource must be created in the frankurt datacenter "default-fra1"