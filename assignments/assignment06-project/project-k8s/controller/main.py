# -*- coding: utf-8 -*-
from fastapi import FastAPI
from kubernetes import client, config
from pathlib import Path

app = FastAPI()

@app.get("/")
def hello_world():
    return "hello world"


# Configs can be set in Configuration class directly or using helper utility
# Example for further configuration: config.load_kube_config(config_file=str(Path("C:/Users/mroenfel/.kube/config")), context="rancher-desktop")
# If you have more than one cluster in your kube config make sure to set the context
config.load_kube_config(context="rancher-desktop")

v1 = client.CoreV1Api()
print("Listing pods with their IPs:")
ret = v1.list_pod_for_all_namespaces(watch=False)
for i in ret.items:
    print("%s\t%s\t%s" % (i.status.pod_ip, i.metadata.namespace, i.metadata.name))