
# Installation & Getting Started
- Install rancher-desktop https://docs.rancherdesktop.io/getting-started/installation/ 
- Recommaned to install WSL, but powershell will work too
- Install kubectl: [windows install](https://kubernetes.io/docs/tasks/tools/install-kubectl-windows/) and [linux install](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)
- Nerdctl will be installed alongside rancher-desktop
- Read
    - [Official k8s about custom resources](https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/custom-resources/)
    - [Offical k8s docs about CRDs](https://kubernetes.io/docs/tasks/extend-kubernetes/custom-resources/custom-resource-definitions/)
    - [Arricel about exactly what we want to do](https://medium.com/@minimaldevops/crds-in-kubernetes-c38037315548)
    - [Pretty neat guide (further below) about Dockerfile and FastAPI](https://hub.docker.com/r/tiangolo/uvicorn-gunicorn-fastapi)

## How to build a container image 
Note: yes, there is a Dockerfile but we do not build a docker image cause it is a company bound synonym for a linux container image. Do not use Docker because we need a licence for it! 

in the directory where the dockerfile is:
``nerdctl build -t python-custom:v0.0.1 .``
Note: yes, the dot at the end is important

The -t flag gives the image a tag. The tag can be whatever you want but should be self explaining. After the colons you can set a version, if you want. 
If you need any dependencies for your controller add them to the requirements.txt


## How to apply changes to kubernetes

``kubectl apply -f FILENAME``

# Project Requirements

- controller deployment
- controller checks for CRD TODO
- controller must manage a state. Persisting the state beyond the container lifetime is not required but definetly a plus
- controller has a HTTP API via which the status of the CRs can be checked (e.g how many )